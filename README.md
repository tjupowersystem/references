# Coding Guide and Reference Material for the Group

This guide covers mandatary practices in programming works in the group. Important topics include:

* [Source code version control](./01_source_code_version_control/README.md)

* [Testing](./02_testing_framework/README.md)

Additional topics will be added.

# 课题组编程工作规范和参考资料

本文描述课题组编程工作中需遵守的规范，包括源码版本管理和测试。后续会添加更多主题。