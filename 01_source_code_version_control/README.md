# Source Code Version Control (with Git)

## Branching Model

We use the Gitflow branching model. The primary reference is the following articles:

* [Original article from Gitflow author](https://nvie.com/posts/a-successful-git-branching-model/)

* [Bitbucket article on Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

* [GitHub article on the GitHub flow](https://guides.github.com/introduction/flow/)

Relevant Git Commands are explained [here](https://www.git-tower.com/learn/git/ebook/en/command-line/advanced-topics/git-flow/).